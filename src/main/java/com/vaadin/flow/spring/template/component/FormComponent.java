package com.vaadin.flow.spring.template.component;

import com.vaadin.flow.component.Component;

import java.io.Serializable;

public class FormComponent implements Serializable {

    private transient String label;

    private transient Component component;

    public FormComponent() {
    }

    public FormComponent(String label, Component component) {
        this.label = label;
        this.component = component;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

}
