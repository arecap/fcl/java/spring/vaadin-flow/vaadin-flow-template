package com.vaadin.flow.spring.template.component;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.spring.eventbus.EventBus;

import java.util.EventObject;

public class EventButton extends Button  {

    private EventObject eventObject;

    public EventButton(EventObject eventObject) {
        this.eventObject = eventObject;
        addClickListener(this::onClick);
    }

    public EventButton(String text, EventObject eventObject) {
        super(text);
        this.eventObject = eventObject;
        addClickListener(this::onClick);
    }

    public EventButton(Component icon, EventObject eventObject) {
        super(icon);
        this.eventObject = eventObject;
        addClickListener(this::onClick);
    }

    public EventButton(String text, Component icon, EventObject eventObject) {
        super(text, icon);
        this.eventObject = eventObject;
        addClickListener(this::onClick);
    }

    private void onClick(ClickEvent<Button> buttonClickEvent) {
        EventBus.notify(eventObject);
    }

}
