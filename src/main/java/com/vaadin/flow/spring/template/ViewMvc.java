package com.vaadin.flow.spring.template;

import com.vaadin.flow.component.HasComponents;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

@Deprecated
public interface ViewMvc {

    @PostConstruct
    default HasComponents getView() {
        Assert.isAssignable(HasComponents.class, this.getClass());
        return (HasComponents) this;
    }

}
