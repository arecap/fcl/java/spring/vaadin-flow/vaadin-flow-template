package com.vaadin.flow.spring.template;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.router.RouteData;
import com.vaadin.flow.spring.template.annotation.IconMark;
import com.vaadin.flow.spring.template.annotation.RouteAddress;
import com.vaadin.flow.spring.template.exception.RouteAddressException;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public final class RouteDataInformation {

    public static String getRouteAddress(RouteData routeData) {
        return Optional.ofNullable(routeData.getNavigationTarget().getAnnotation(RouteAddress.class))
                .map(RouteAddress::value).orElse("");
    }

    public static Predicate<RouteData> getRoutesForPrefix(String prefix) {
        return p -> p.getUrl().startsWith(prefix);
    }

    public static Comparator<RouteData> compareByRouteAddress() {
        return Comparator.comparing(RouteDataInformation::getRouteAddress);
    }

    public static List<RouteData> getRoutesByPrefixSortedByRouteAddress(String prefix) {
        return UI.getCurrent().getRouter().getRoutes().stream()
                .filter(getRoutesForPrefix(prefix)).sorted(compareByRouteAddress()).collect(Collectors.toList());
    }

    public static Map<String, String> getRouteAddressPathFragments(String prefix) {
        Map<String, String> addressPathFragments = new LinkedHashMap<>();
        getRoutesByPrefixSortedByRouteAddress(prefix).stream()
                .forEach(routeData -> processAddressesFragments(addressPathFragments, routeData));
        return addressPathFragments;
    }

    public static void processAddressesFragments(Map<String, String> addressPathFragments, RouteData routeData) {
        checkRouteAddressFormatDefinition(routeData);
        processAddressesFragments(addressPathFragments, routeData, Paths.get(getRouteAddress(routeData)), Paths.get(routeData.getUrl()));
    }

    public static void processAddressesFragments(Map<String, String> addressPathFragments, RouteData routeData, Path address, Path path) {
        int fragments = getRouteAddressLength(routeData); int i = 0;
        do {
            String subpathSeparator = (fragments > 1 ? "/" : "");
            processAddressesFragments(addressPathFragments,
                    address.subpath(0, ++i).toString() + subpathSeparator,
                    path.subpath(0, i).toString() + subpathSeparator);
        } while (--fragments > 0);
    }

    public static void processAddressesFragments(Map<String, String> addressPathFragments, String addressFragment, String pathFragment) {
        if(addressPathFragments.get(addressFragment) == null) {
            addressPathFragments.put(addressFragment, pathFragment);
        } else {
            checkAddressFragmentPathFragment(addressPathFragments, addressFragment, pathFragment);
        }
    }

    public static void checkRouteAddressFormatDefinition(RouteData routeData) {
        if(getRouteAddressLength(routeData)!= getRoutePathLength(routeData)) {
            throw new RouteAddressException();
        }
    }

    public static void checkAddressFragmentPathFragment(Map<String, String> addressPathFragments, String addressFragment, String pathFragment) {
        if(!addressPathFragments.get(addressFragment).equalsIgnoreCase(pathFragment)) {
            throw new RouteAddressException();
        }
    }

    public static int getRoutePathLength(RouteData routeData) {
        return routeData.getUrl().split("/").length;
    }

    public static int getRouteAddressLength(RouteData routeData) {
        return getRouteAddress(routeData).split("/").length;
    }

    public static Icon getIconMark(String path) {
        path = path.endsWith("/") ? path.substring(0, path.length() - 1) : path;
        List<IconMark> iconBookmarks = AnnotatedElementUtils
                .findMergedRepeatableAnnotations(getRoutesByPrefixSortedByRouteAddress(path)
                        .get(0).getNavigationTarget(), IconMark.class).stream().collect(Collectors.toList());

        Assert.state(Math.max(0, StringUtils.countOccurrencesOf(path, "/")) < iconBookmarks.size(),
                "Route Icon Bookmarks definition error!");
        return iconBookmarks.get(Math.max(0, StringUtils.countOccurrencesOf(path, "/"))).value().create();
    }

}
