package com.vaadin.flow.spring.template.annotation;


import java.lang.annotation.*;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Documented
public @interface RouteAddress {

    String value() default "";

}
