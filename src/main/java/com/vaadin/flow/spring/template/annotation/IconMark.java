package com.vaadin.flow.spring.template.annotation;

import com.vaadin.flow.component.icon.VaadinIcon;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(RouteIconBookmarks.class)
@Documented
public @interface IconMark {

    @AliasFor("icon")
    VaadinIcon value() default VaadinIcon.LAPTOP;

    @AliasFor("value")
    VaadinIcon icon() default VaadinIcon.LAPTOP;

}
