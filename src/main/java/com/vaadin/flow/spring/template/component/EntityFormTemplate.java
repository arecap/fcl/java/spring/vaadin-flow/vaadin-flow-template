package com.vaadin.flow.spring.template.component;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.data.binder.Binder;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.Assert;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public abstract class EntityFormTemplate<T> extends FormLayout {

    public static final ReflectionUtils.FieldFilter FORM_ITEM_COMPONENT_FIELD = field ->
            (AnnotationUtils.findAnnotation(field, FormItemComponent.class) != null);

    private List<EventButton> events = new ArrayList<>();

    private List<FormComponent> components = new ArrayList<>();

    private Binder<T> binder = new Binder<>();

    private HorizontalLayout eventsLayout =  new HorizontalLayout();

    public void setup(boolean before) {
        if(before)
            setupEventButtonsLayout();
        constructFormComponents();
        components.stream().forEach(this::addFormComponent);
        events.stream().forEach(this::addFormEvent);
        if(!before)
            setupEventButtonsLayout();
    }

    public void setBean(T bean) {
        binder = new Binder<T>((Class<T>) bean.getClass());
        binder.bindInstanceFields(this);
        binder.setBean(bean);
    }

    public T getBean() {
        return binder.getBean();
    }

    public void addFormEvent(EventButton eventButton) {
        eventsLayout.add(eventButton);
    }

    protected void setupEventButtonsLayout() {
        eventsLayout.setSpacing(false);
        eventsLayout.setWidthFull();
        add(eventsLayout);
    }

    private void constructFormComponents() {
        ReflectionUtils.doWithFields(getClass(), this::constructFormComponent, FORM_ITEM_COMPONENT_FIELD);
    }

    private void constructFormComponent(Field field)  {
        Assert.isAssignable(Component.class, field.getType());
        try {
            Component componentInstance = (Component) field.getType().newInstance();
            setFieldComponentInstance(field, componentInstance);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        }
    }

    private void setFieldComponentInstance(Field field, Component component) throws IllegalAccessException {
        FormItemComponent itemComponent = AnnotatedElementUtils.getMergedAnnotation(field, FormItemComponent.class);
        setFieldComponentInstance(field, component, itemComponent);
    }

    private void setFieldComponentInstance(Field field, Component component, FormItemComponent itemComponent) throws IllegalAccessException {
        boolean acc = field.isAccessible();
        field.setAccessible(true);
        field.set(this, component);
        field.setAccessible(acc);
        components.add(new FormComponent(itemComponent.label(), component));
    }


    private void addFormComponent(FormComponent formComponent) {
        addFormItem(formComponent.getComponent(), formComponent.getLabel());
    }


}
