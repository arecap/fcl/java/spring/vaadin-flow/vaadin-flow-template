package com.vaadin.flow.spring.template.annotation;

import com.vaadin.flow.spring.annotation.UIScope;
import com.vaadin.flow.spring.template.ViewMvc;
import org.contextualj.lang.annotation.expression.SourceType;
import org.springframework.cop.annotation.ContextOriented;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@ContextOriented
@UIScope
@SourceType(ViewMvc.class)
@Documented
@Deprecated
public @interface GuiController {


}
