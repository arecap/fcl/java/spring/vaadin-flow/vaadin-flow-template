package com.vaadin.flow.spring.template.annotation;

import java.lang.annotation.*;

@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RouteIconBookmarks {

    IconMark[] value();

}
