package com.vaadin.flow.spring.template.component;

import java.lang.annotation.*;

@Target({ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface FormItemComponent {

    String label();

}
